Semestrální projekt Ondřeje Kyzra.

Návod na použití je v záložce Wiki.

Téma mojí semestrální práce je 2D RPG Roguelike game engine. Level hry bude obsahovat ručně
vytvořené místnosti s překážkami, nepřáteli, truhlami (s předměty) a hroty. Hráč bude procházet několik
místností v rámci jednoho levelu, kde bude bojovat s nepřáteli a získávat předměty. Předměty budou hráči
vylepšovat statistiky jako damage, health, firerate atd. Podle časové náročnosti budu postupně dodělávat
features jako přidávání itemů/nepřátel/místností/překážek nebo nějaké lepší AI nepřátel.

Hra je plně modulární, lze snadno přidat předměty, místnosti i levely.
