package cz.fel.rpg.Level;

import cz.fel.rpg.GameObjects.Entities.EntityController;
import cz.fel.rpg.GameObjects.Entities.EntityModel;
import cz.fel.rpg.GameObjects.GameObject;

public class TestController extends EntityController {

    public TestController(Integer health, Integer attack, Double speed){
        super(health, attack, speed);
    }

    private TestController(String texture, Integer x, Integer y, EntityModel model){
        super(texture, x, y, model);
    }

    public void moveInDirection(Integer x, Integer y){
        move(x, y);
    }

    public void attackInDirection(Integer x, Integer y){
        attack(x, y);
    }

    @Override
    public GameObject createGameObject(String texture, Integer x, Integer y) {
        return new TestController(texture, x, y, entityModel);
    }

    @Override
    public void run() {

    }
}
