package cz.fel.rpg.Level;

import cz.fel.rpg.GameObjects.Chest;
import cz.fel.rpg.GameObjects.Entities.EntityController;
import cz.fel.rpg.GameObjects.Entities.PlayerController;
import cz.fel.rpg.GameObjects.GameObject;
import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

import static cz.fel.rpg.Utils.Utils.ROOM_X_CAPACITY;
import static cz.fel.rpg.Utils.Utils.ROOM_Y_CAPACITY;

public class RoomTest extends TestCase {

    /**
     * Tests if the room implements movement of entities properly.
     */
    public void testMoveEntity() {
        Integer entity1X = 0;
        Integer entity1Y = 0;

        Integer entity2X = 0;
        Integer entity2Y = 1;

        TestController blueprint = new TestController(10, 5, 2D);
        TestController entity1 = (TestController) blueprint.createGameObject("", entity1X, entity1Y);
        TestController entity2 = (TestController) blueprint.createGameObject("", entity2X, entity2Y);
        List<EntityController> enemies = new ArrayList<>();
        enemies.add(entity1);
        enemies.add(entity2);
        List<List<GameObject>> objectAlignment = new ArrayList<>();

        for (int i = 0; i < ROOM_Y_CAPACITY; i++) {
            objectAlignment.add(new ArrayList<>());
            for (int j = 0; j < ROOM_X_CAPACITY; j++) {
                objectAlignment.get(i).add(null);
            }
        }

        objectAlignment.get(entity1.getY()).set(entity1.getX(), entity1);
        objectAlignment.get(entity2.getY()).set(entity2.getX(), entity2);

        Room room = new Room(objectAlignment, enemies, null, 0);

        entity1.setCurrRoom(room);
        entity2.setCurrRoom(room);

        entity1.moveInDirection(0, 1);
        assertEquals("Entity went through another entity!", entity1, room.getRoomObjects().get(entity1.getY()).get(entity1.getX()));
        entity2.moveInDirection(0, -1);
        assertEquals("Entity went through another entity!", entity2, room.getRoomObjects().get(entity2.getY()).get(entity2.getX()));

        entity1.moveInDirection(-1, 0);
        assertEquals("Entity went out of bounds!", entity1, room.getRoomObjects().get(entity1.getY()).get(entity1.getX()));

        entity2.moveInDirection(1, 0);
        entity2.moveInDirection(0, -1);
        assertEquals("Entity doesn't move properly!", room.getRoomObjects().get(0).get(1), entity2);
    }

    /**
     * Tests if the room implements deleting an entity properly.
     */
    public void testDeleteEntity() {
        TestController blueprint = new TestController(10, 5, 2D);
        TestController entity1 = (TestController) blueprint.createGameObject("", 0, 0);
        List<EntityController> enemies = new ArrayList<>();
        enemies.add(entity1);
        List<List<GameObject>> objectAlignment = new ArrayList<>();

        for (int i = 0; i < ROOM_Y_CAPACITY; i++) {
            objectAlignment.add(new ArrayList<>());
            for (int j = 0; j < ROOM_X_CAPACITY; j++) {
                objectAlignment.get(i).add(null);
            }
        }

        objectAlignment.get(0).set(0, entity1);
        Room room = new Room(objectAlignment, enemies, null, 0);
        entity1.setCurrRoom(room);

        room.deleteEntity(entity1);

        assertNull("Entity has not been deleted!", room.getRoomObjects().get(0).get(0));
    }

    /**
     * Tests if the room implements killing all enemies properly.
     */
    public void testKillAllEnemies() {
        Integer entity1X = 0;
        Integer entity1Y = 0;

        Integer entity2X = 0;
        Integer entity2Y = 1;

        TestController blueprint = new TestController(10, 5, 2D);
        TestController entity1 = (TestController) blueprint.createGameObject("", entity1X, entity1Y);
        TestController entity2 = (TestController) blueprint.createGameObject("", entity2X, entity2Y);
        List<EntityController> enemies = new ArrayList<>();
        enemies.add(entity1);
        enemies.add(entity2);
        List<List<GameObject>> objectAlignment = new ArrayList<>();

        for (int i = 0; i < ROOM_Y_CAPACITY; i++) {
            objectAlignment.add(new ArrayList<>());
            for (int j = 0; j < ROOM_X_CAPACITY; j++) {
                objectAlignment.get(i).add(null);
            }
        }

        objectAlignment.get(entity1.getY()).set(entity1.getX(), entity1);
        objectAlignment.get(entity2.getY()).set(entity2.getX(), entity2);

        Room room = new Room(objectAlignment, enemies, null, 0);

        entity1.setCurrRoom(room);
        entity2.setCurrRoom(room);

        room.killAllEnemies();

        assertEquals("There are still enemies in enemies list after killing them!", 0, (int) room.getEnemiesSize());
        assertNull("An enemy was not deleted!", room.getRoomObjects().get(entity1.getY()).get(entity1.getX()));
        assertNull("The enemies were not deleted!", room.getRoomObjects().get(entity2.getY()).get(entity2.getX()));

    }

    /**
     * Tests if the room implements attacking entities properly.
     */
    public void testAttackGameObject() {
        Integer entity1X = 0;
        Integer entity1Y = 0;

        Integer entity2X = 0;
        Integer entity2Y = 1;

        TestController blueprint = new TestController(10, 5, 2D);
        TestController entity1 = (TestController) blueprint.createGameObject("", entity1X, entity1Y);
        TestController entity2 = (TestController) blueprint.createGameObject("", entity2X, entity2Y);
        List<EntityController> enemies = new ArrayList<>();
        enemies.add(entity1);
        enemies.add(entity2);
        List<List<GameObject>> objectAlignment = new ArrayList<>();

        for (int i = 0; i < ROOM_Y_CAPACITY; i++) {
            objectAlignment.add(new ArrayList<>());
            for (int j = 0; j < ROOM_X_CAPACITY; j++) {
                objectAlignment.get(i).add(null);
            }
        }

        objectAlignment.get(entity1.getY()).set(entity1.getX(), entity1);
        objectAlignment.get(entity2.getY()).set(entity2.getX(), entity2);

        Room room = new Room(objectAlignment, enemies, null, 0);

        entity1.setCurrRoom(room);
        entity2.setCurrRoom(room);

        entity1.attackInDirection(0, 1);
        assertEquals("After attacking entity the health is wrong!", 5, (int) entity2.getHealth());
        entity1.attackInDirection(0, 1);
        assertEquals("After attacking entity the health is wrong!", 0, (int) entity2.getHealth());

        entity2.attackInDirection(0, -1);
        assertEquals("Dead entities can't attack!", 10, (int) entity1.getHealth());
    }

    /**
     * Tests if the room implements interaction with Interactive properly.
     */
    public void testInteractWithGameObject() {
        Integer entity1X = 1;
        Integer entity1Y = 1;

        PlayerController player = (PlayerController) new PlayerController(10, 5, 2D).createGameObject("", entity1X, entity1Y);

        Chest chestUp = (Chest) new Chest().createGameObject("", 1,0);
        Chest chestDown = (Chest) new Chest().createGameObject("", 1,2);
        Chest chestLeft = (Chest) new Chest().createGameObject("", 0,1);
        Chest chestRight = (Chest) new Chest().createGameObject("", 2,1);

        List<List<GameObject>> objectAlignment = new ArrayList<>();

        for (int i = 0; i < ROOM_Y_CAPACITY; i++) {
            objectAlignment.add(new ArrayList<>());
            for (int j = 0; j < ROOM_X_CAPACITY; j++) {
                objectAlignment.get(i).add(null);
            }
        }

        Room room = new Room(objectAlignment, null, null, 0);
        player.setCurrRoom(room);

        room.placeGameObject(player, player.getX(), player.getY());
        room.placeGameObject(chestUp, chestUp.getX(), chestUp.getY());
        room.placeGameObject(chestDown, chestDown.getX(), chestDown.getY());
        room.placeGameObject(chestLeft, chestLeft.getX(), chestLeft.getY());
        room.placeGameObject(chestRight, chestRight.getX(), chestRight.getY());

        player.interactWithSurrounding();

        assertNull("Chest above player has not been opened!", room.getRoomObjects().get(chestUp.getY()).get(chestUp.getX()));
        assertNull("Chest below player has not been opened!", room.getRoomObjects().get(chestDown.getY()).get(chestDown.getX()));
        assertNull("Chest to the left of the player has not been opened!", room.getRoomObjects().get(chestLeft.getY()).get(chestLeft.getX()));
        assertNull("Chest to the right of the player has not been opened!", room.getRoomObjects().get(chestRight.getY()).get(chestRight.getX()));
    }
}