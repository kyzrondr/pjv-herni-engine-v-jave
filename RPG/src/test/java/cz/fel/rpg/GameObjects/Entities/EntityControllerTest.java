package cz.fel.rpg.GameObjects.Entities;

import cz.fel.rpg.Level.TestController;
import junit.framework.TestCase;

public class EntityControllerTest extends TestCase {

    /**
     * Tests if the the EntityController changes it stats correctly.
     */
    public void testChangeStats() {
        TestController blueprint = new TestController(10, 5, 2D);
        TestController entity1 = (TestController) blueprint.createGameObject("", 0, 0);

        entity1.changeStats(-5, 0,0,0D, 0);
        assertEquals("Entity didn't lower it's health after losing maxHealth!", 5, (int) entity1.getHealth());
        assertEquals("Entity didn't lower it's maxHealth!", 5, (int) entity1.getMaxHealth());

        entity1.changeStats(-10, 0,0,0D, 0);
        assertEquals("Entity didn't set health to 1 after it getting maxHealth below 0!", 1, (int) entity1.getHealth());
        assertEquals("Entity didn't set maxHealth to 1 after it getting below 0!", 1, (int) entity1.getMaxHealth());

        entity1.changeStats(9, 0,0,0D, 0);
        assertEquals("Entity didn't heal itself when gaining more maxHealth!", 10, (int) entity1.getHealth());

        entity1.changeStats(0, 10,0,0D, 0);
        assertEquals("Entity healed itself over maxHealth!", 10, (int) entity1.getHealth());

        entity1.changeStats(0, 0,-20,0D, 0);
        assertEquals("Entity didn't lower it's attack to 1 after getting below 0!", 1, (int) entity1.getAttack());

        entity1.changeStats(0, 0,0,-3D, 0);
        assertEquals("Entity didn't lower it's attackSpeed to 0.1 after getting below 0!", 0.1D, entity1.getAttackSpeed());

        entity1.changeStats(0, -20,0,0D, 0);
        assertFalse("Entity didn't die after getting health below 0!", entity1.entityModel.getAlive());
    }
}