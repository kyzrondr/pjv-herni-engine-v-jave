package cz.fel.rpg.GameObjects;

import cz.fel.rpg.GameObjects.Entities.PlayerController;
import cz.fel.rpg.Level.LevelController;
import junit.framework.TestCase;

public class GameExitTest extends TestCase {

    /**
     * Tests if the game and level has ended after player stepping on GameExit.
     */
    public void testSteppedOn() {
        LevelController levelController = new LevelController();
        GameExit gameExit = (GameExit) new GameExit().createGameObject("", 0, 0);
        PlayerController playerController = new PlayerController(10,0 ,0D);

        playerController.setCurrLevel(levelController);
        gameExit.steppedOn(playerController);

        assertFalse("GameExit has not exited the level!", levelController.getStayInLevel());
        assertFalse("GameExit has not exited the game!", levelController.getStayInGame());
    }
}