package cz.fel.rpg.GameObjects;

import cz.fel.rpg.GameObjects.Entities.PlayerController;
import cz.fel.rpg.Level.LevelController;
import junit.framework.TestCase;

public class LevelExitTest extends TestCase {

    /**
     * Tests if the level has ended after player stepping on LevelExit.
     */
    public void testSteppedOn() {
        LevelController levelController = new LevelController();
        LevelExit levelExit = (LevelExit) new LevelExit().createGameObject("", 0, 0);
        PlayerController playerController = new PlayerController(10,0 ,0D);

        playerController.setCurrLevel(levelController);
        levelExit.steppedOn(playerController);

        assertFalse("LevelExit has not exited the level!", levelController.getStayInLevel());
    }
}