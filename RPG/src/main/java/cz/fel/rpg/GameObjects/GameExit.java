package cz.fel.rpg.GameObjects;

import cz.fel.rpg.GameObjects.Entities.EntityController;
import cz.fel.rpg.GameObjects.Entities.PlayerController;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A GameObject that when it's SteppedOn it end the game.
 */
public class GameExit extends GameObject implements StepOnAble {

    /**
     * A blueprint constructor for GameObjectIDs.
     */
    public GameExit() {
    }

    /**
     * Calls the GameObject constructor.
     *
     * @param texture Path to the texture the GameObject uses.
     * @param x       X coordinate of the GameObject in the room.
     * @param y       Y coordinate of the GameObject in the room.
     */
    private GameExit(String texture, Integer x, Integer y) {
        super(texture, x, y);
    }


    /**
     * @param texture Path to the texture the GameObject uses.
     * @param x       X coordinate of the GameObject in the room.
     * @param y       Y coordinate of the GameObject in the room.
     * @return Returns a properly created GameExit.
     */
    @Override
    public GameObject createGameObject(String texture, Integer x, Integer y) {
        return new GameExit(texture, x, y);
    }

    /**
     * When the entity that stepped on this is the player it ends the game.
     *
     * @param entity The entity that stepped on this GameObject.
     */
    @Override
    public void steppedOn(EntityController entity) {
        if (entity instanceof PlayerController) {
            ((PlayerController) entity).getCurrLevel().setStayInLevel(false);
            ((PlayerController) entity).getCurrLevel().setStayInGame(false);
            Logger.getLogger(getClass().getName()).log(Level.INFO, "Player has exited the game.");
        }
    }
}

