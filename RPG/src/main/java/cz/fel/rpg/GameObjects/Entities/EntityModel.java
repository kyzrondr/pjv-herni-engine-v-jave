package cz.fel.rpg.GameObjects.Entities;


/**
 * A model for storing the stats of an Entity.
 */
public class EntityModel {
    private EntityController entityController;
    private Integer maxHealth;
    private Integer health;
    private Integer attack;
    private Double attackSpeed;
    private Integer numOfKeys = 0;
    private Boolean alive = true;

    /**
     * @param health      An amount of health an Entity is supposed to have.
     * @param attack      An amount of attack an Entity is supposed to have.
     * @param attackSpeed An amount of attackSpeed/speed an Entity is supposed to have.
     */
    public EntityModel(Integer health, Integer attack, Double attackSpeed) {
        this.maxHealth = health;
        this.health = health;
        this.attack = attack;
        this.attackSpeed = attackSpeed;
    }

    public Integer getMaxHealth() {
        return maxHealth;
    }

    public Integer getHealth() {
        return health;
    }

    public Integer getAttack() {
        return attack;
    }

    public Double getAttackSpeed() {
        return attackSpeed;
    }

    public Boolean getAlive() {
        return alive;
    }

    public Integer getNumOfKeys() {
        return numOfKeys;
    }

    public void setMaxHealth(Integer maxHealth) {
        this.maxHealth = maxHealth;
    }

    public void setHealth(Integer health) {
        this.health = health;
    }

    public void setAttack(Integer attack) {
        this.attack = attack;
    }

    public void setAttackSpeed(Double attackSpeed) {
        this.attackSpeed = attackSpeed;
    }

    public void setAlive(Boolean alive) {
        this.alive = alive;
    }

    public void setNumOfKeys(Integer numOfKeys) {
        this.numOfKeys = numOfKeys;
    }

    public void setEntityController(EntityController entityController) {
        this.entityController = entityController;
    }

}
