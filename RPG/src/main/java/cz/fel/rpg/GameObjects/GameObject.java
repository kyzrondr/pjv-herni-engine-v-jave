package cz.fel.rpg.GameObjects;

import javax.swing.*;

/**
 * A class to unify all the render able objects that are supposed to show up in a room.
 */
public abstract class GameObject {
    protected ImageIcon texture;
    protected Integer x;
    protected Integer y;

    /**
     * A blueprint constructor for GameObjectIDs.
     */
    protected GameObject() {
    }

    /**
     * Creates a proper GameObject with a texture and coordinates.
     *
     * @param texture Path to the texture the GameObject uses.
     * @param x       X coordinate of the GameObject in the room.
     * @param y       Y coordinate of the GameObject in the room.
     */
    protected GameObject(String texture, Integer x, Integer y) {
        this.texture = new ImageIcon(texture);
        this.x = x;
        this.y = y;
    }

    /**
     * Used by it's children to create a proper version of itself from a blueprint.
     *
     * @param texture Path to the texture the GameObject uses.
     * @param x       X coordinate of the GameObject in the room.
     * @param y       Y coordinate of the GameObject in the room.
     * @return Returns a properly created GameObject.
     */
    public abstract GameObject createGameObject(String texture, Integer x, Integer y);

    public ImageIcon getTexture() {
        return texture;
    }

    public Integer getX() {
        return x;
    }

    public Integer getY() {
        return y;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public void setY(Integer y) {
        this.y = y;
    }
}
