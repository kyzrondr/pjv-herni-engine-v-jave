package cz.fel.rpg.GameObjects.Entities;

import cz.fel.rpg.GameObjects.GameObject;
import cz.fel.rpg.Level.LevelController;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A PlayerController that the person playing controls.
 */
public class PlayerController extends EntityController {
    private LevelController currLevel;
    private final Integer DEATH_DELAY = 3000; // Amount of time the player waits when he dies in ms.

    /**
     * A player blueprint that calls the EntityController blueprint.
     *
     * @param health Amount of health the player has.
     * @param attack Amount of attack the player has.
     * @param speed  Amount of attackSpeed the player has.
     */
    public PlayerController(Integer health, Integer attack, Double speed) {
        super(health, attack, speed);
    }

    /**
     * Call the proper constructor of EntityController.
     *
     * @param texture Path to the texture the GameObject uses.
     * @param x       X coordinate of the GameObject in the room.
     * @param y       Y coordinate of the GameObject in the room.
     * @param model   A template model from which the health, attack and attackSpeed/speed.
     */
    private PlayerController(String texture, Integer x, Integer y, EntityModel model) {
        super(texture, x, y, model);
    }

    /**
     * @param texture Path to the texture the GameObject uses.
     * @param x       X coordinate of the GameObject in the room.
     * @param y       Y coordinate of the GameObject in the room.
     * @return Returns a properly created PlayerController.
     */
    @Override
    public GameObject createGameObject(String texture, Integer x, Integer y) {
        return new PlayerController(texture, x, y, entityModel);
    }

    /**
     * Interacts with a tile above, below, right and left of the player.
     */
    public void interactWithSurrounding() {
        // Interacts with tile above player
        currRoom.interactWithGameObject(this, x, y - 1);

        // Interacts with tile below player
        currRoom.interactWithGameObject(this, x, y + 1);

        // Interacts with tile right of player
        currRoom.interactWithGameObject(this, x - 1, y);

        // Interacts with tile left of player
        currRoom.interactWithGameObject(this, x + 1, y);

    }

    /**
     * Used for delaying the players attack.
     */
    @Override
    public void run() {
        while (entityModel.getAlive()) {
            try {
                // if player wants to attack it attacks and then waits
                if (attackingCoordsOffset[0] != 0 || attackingCoordsOffset[1] != 0) {
                    attacking = true;
                    attack(attackingCoordsOffset[0], attackingCoordsOffset[1]);
                    Thread.sleep((long) (SECONDS_IN_MS / entityModel.getAttackSpeed()));
                    attackingCoordsOffset[0] = attackingCoordsOffset[1] = 0;
                    attacking = false;
                }

                Thread.sleep(0);    // Something has to be here because without it the while cycle doesn't execute ¯\_(ツ)_/¯
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * Logs players death, wait a bit for the Game Over screen, kill all enemies in the room (ends their Threads) and exit level.
     */
    @Override
    public void death() {
        Logger.getLogger(getClass().getName()).log(Level.INFO, "Player has died. Resetting to menu.");
        currRoom.killAllEnemies();
        entityModel.setAlive(false);
        try {
            Thread.sleep(DEATH_DELAY);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        currLevel.setStayInLevel(false);
    }

    public LevelController getCurrLevel() {
        return currLevel;
    }

    public Boolean getAlive() {
        return entityModel.getAlive();
    }

    public void setCurrLevel(LevelController currLevel) {
        this.currLevel = currLevel;
    }
}
