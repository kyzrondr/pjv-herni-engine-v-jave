package cz.fel.rpg.GameObjects;

import cz.fel.rpg.GameObjects.Entities.EntityController;
import cz.fel.rpg.GameObjects.Entities.PlayerController;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A GameObject which end the current level.
 */
public class LevelExit extends GameObject implements StepOnAble {

    /**
     * A blueprint constructor for GameObjectIDs.
     */
    public LevelExit() {
    }

    /**
     * Calls the GameObject constructor.
     *
     * @param texture Path to the texture the GameObject uses.
     * @param x       X coordinate of the GameObject in the room.
     * @param y       Y coordinate of the GameObject in the room.
     */
    private LevelExit(String texture, Integer x, Integer y) {
        super(texture, x, y);
    }

    /**
     * @param texture Path to the texture the GameObject uses.
     * @param x       X coordinate of the GameObject in the room.
     * @param y       Y coordinate of the GameObject in the room.
     * @return Returns a properly created LevelExit.
     */
    @Override
    public GameObject createGameObject(String texture, Integer x, Integer y) {
        return new LevelExit(texture, x, y);
    }

    /**
     * When the Player steps on this it ends the current level.
     *
     * @param entity The entity that stepped on this GameObject.
     */
    @Override
    public void steppedOn(EntityController entity) {
        if (entity instanceof PlayerController) {
            ((PlayerController) entity).getCurrLevel().setStayInLevel(false);
            Logger.getLogger(getClass().getName()).log(Level.INFO, "Player left the current level.");
        }
    }
}
