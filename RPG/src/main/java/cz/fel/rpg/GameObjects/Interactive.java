package cz.fel.rpg.GameObjects;

import cz.fel.rpg.GameObjects.Entities.PlayerController;
import cz.fel.rpg.Level.Room;

public interface Interactive {

    /**
     * @param room   The room the GameObject is located in.
     * @param player The PlayerController that interacted with the GameObject.
     */
    void interact(Room room, PlayerController player);
}
