package cz.fel.rpg.GameObjects.Entities.EnemyControllers;

import cz.fel.rpg.GameObjects.Entities.EntityController;
import cz.fel.rpg.GameObjects.Entities.EntityModel;
import cz.fel.rpg.GameObjects.Entities.OnDeath;
import cz.fel.rpg.GameObjects.GameObject;

import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Wonders randomly (preferring the X axis) and doesn't attack.
 */
public class SlimeController extends EntityController implements OnDeath {

    /**
     * A Slime blueprint that calls the EntityController blueprint.
     *
     * @param health Amount of health the player has.
     * @param attack Amount of attack the player has.
     * @param speed  Amount of attackSpeed the player has.
     */
    public SlimeController(Integer health, Integer attack, Double speed) {
        super(health, attack, speed);
    }

    /**
     * Call the proper constructor of EntityController.
     *
     * @param texture Path to the texture the GameObject uses.
     * @param x       X coordinate of the GameObject in the room.
     * @param y       Y coordinate of the GameObject in the room.
     * @param model   A template model from which the health, attack and attackSpeed/speed.
     */
    private SlimeController(String texture, Integer x, Integer y, EntityModel model) {
        super(texture, x, y, model);
    }

    /**
     * Does an moves while it's alive.
     */
    @Override
    public void run() {
        Integer possibleNums = 3;

        while (entityModel.getAlive()) {

            Random rand = new Random();
            int x = rand.nextInt(possibleNums) - 1; // Creates a random number in interval <-1,1>
            int y = 0;
            if (x == 0) {
                y = rand.nextInt(possibleNums) - 1; // Creates a random number in interval <-1,1>
            }
            move(x, y);

            try {
                // waits for a random time between AttackSpeed and AttackSpeed * 2
                Thread.sleep((long) (SECONDS_IN_MS / (entityModel.getAttackSpeed() + (rand.nextDouble() % entityModel.getAttackSpeed()))));
            } catch (InterruptedException e) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Thread Interrupted error!");
                e.printStackTrace();
            }
        }
    }

    /**
     * @param texture Path to the texture the GameObject uses.
     * @param x       X coordinate of the GameObject in the room.
     * @param y       Y coordinate of the GameObject in the room.
     * @return Returns a properly created SlimeController.
     */
    @Override
    public GameObject createGameObject(String texture, Integer x, Integer y) {
        return new SlimeController(texture, x, y, entityModel);
    }
}
