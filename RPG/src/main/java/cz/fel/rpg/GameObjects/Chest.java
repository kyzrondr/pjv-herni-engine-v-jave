package cz.fel.rpg.GameObjects;

import cz.fel.rpg.GameObjects.Entities.PlayerController;
import cz.fel.rpg.Level.Room;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A GameObject which holds a Item that is placed instead of itself when it's interacted with.
 */
public class Chest extends GameObject implements Interactive {
    private Item item = null;

    /**
     * A blueprint constructor for GameObjectIDs.
     */
    public Chest() {
    }

    /**
     * Calls the GameObject constructor.
     *
     * @param texture Path to the texture the GameObject uses.
     * @param x       X coordinate of the GameObject in the room.
     * @param y       Y coordinate of the GameObject in the room.
     */
    private Chest(String texture, Integer x, Integer y) {
        super(texture, x, y);
    }

    /**
     * @param texture Path to the texture the GameObject uses.
     * @param x       X coordinate of the GameObject in the room.
     * @param y       Y coordinate of the GameObject in the room.
     * @return Returns a properly created Chest.
     */
    @Override
    public GameObject createGameObject(String texture, Integer x, Integer y) {
        return new Chest(texture, x, y);
    }

    /**
     * Places an Item instead of itself.
     *
     * @param room   The current room the chest is placed in.
     * @param player The PlayerController that opened the Chest.
     */
    @Override
    public void interact(Room room, PlayerController player) {
        room.placeGameObject(item, x, y);
        if (item == null) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING, getClass().getName() + " is empty.");
        } else {
            Logger.getLogger(getClass().getName()).log(Level.INFO, "Player opened a chest with an Item.");
        }
    }

    public void setItem(Item item) {
        this.item = item;
    }
}
