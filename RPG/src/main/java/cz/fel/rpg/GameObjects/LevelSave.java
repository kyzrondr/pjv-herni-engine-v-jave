package cz.fel.rpg.GameObjects;

import cz.fel.rpg.GameObjects.Entities.EntityController;
import cz.fel.rpg.GameObjects.Entities.PlayerController;
import cz.fel.rpg.Utils.Utils;

/**
 *  Upon stepping on this object it saves the current levelID and player State and exits the game.
 */
public class LevelSave extends GameObject implements StepOnAble{

    /**
     * A blueprint constructor for GameObjectIDs.
     */
    public LevelSave(){
    }

    /**
     * Calls the GameObject constructor.
     *
     * @param texture Path to the texture the GameObject uses.
     * @param x       X coordinate of the GameObject in the room.
     * @param y       Y coordinate of the GameObject in the room.
     */
    private LevelSave(String texture, Integer x, Integer y){
        super(texture, x, y);
    }


    /**
     * @param texture Path to the texture the GameObject uses.
     * @param x       X coordinate of the GameObject in the room.
     * @param y       Y coordinate of the GameObject in the room.
     * @return Returns a properly created LevelSave.
     */
    @Override
    public GameObject createGameObject(String texture, Integer x, Integer y) {
        return new LevelSave(texture, x, y);
    }

    /**
     * Saves the current levelID and player State.
     *
     * @param entity The entity that stepped on the GameObject
     */
    @Override
    public void steppedOn(EntityController entity) {
        if (entity instanceof PlayerController){
            PlayerController player = (PlayerController) entity;
            Utils.saveLevel(player.getCurrLevel().getLevelID(), player);
            player.getCurrLevel().setStayInLevel(false);
            player.getCurrLevel().setStayInGame(false);
        }
    }
}
