package cz.fel.rpg.GameObjects.Entities;

import cz.fel.rpg.GameObjects.GameObject;
import cz.fel.rpg.Level.Room;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A basic EntityController which is extended to create a new EnemyController (examples: SlimeController, SkeletonController).
 */
public abstract class EntityController extends GameObject implements Runnable, OnDeath {
    protected final Integer SECONDS_IN_MS = 1000;   // Used for calculating the delay of an entity
    protected EntityModel entityModel;
    protected Room currRoom;
    protected final Integer[] attackingCoordsOffset = {0, 0};    // Used as a next planned attack while Thread waits
    protected Boolean attacking = false;
    protected PlayerController player;


    /**
     * Creates an entity model for storing the values of an given enemy. Basically a blueprint for an enemy.
     *
     * @param health Amount of health a given entity has.
     * @param attack Amount of attack a given entity has.
     * @param speed  Amount of attackSpeed/speed a given entity has.
     */
    protected EntityController(Integer health, Integer attack, Double speed) {
        entityModel = new EntityModel(health, attack, speed);
    }

    /**
     * Creates a proper EntityController GameObject and an EntityModel for it from given EntityModel.
     *
     * @param texture Path to the texture the GameObject uses.
     * @param x       X coordinate of the GameObject in the room.
     * @param y       Y coordinate of the GameObject in the room.
     * @param model   A template model from which the health, attack and attackSpeed/speed.
     */
    protected EntityController(String texture, Integer x, Integer y, EntityModel model) {
        super(texture, x, y);
        entityModel = new EntityModel(model.getHealth(), model.getAttack(), model.getAttackSpeed());
        entityModel.setEntityController(this);
    }

    /**
     * Attacks a tile in the room that the entity is currently located.
     *
     * @param offsetX The X axis offset used to calculate the coordinates of the tile where entity wants to attack.
     * @param offsetY The Y axis offset used to calculate the coordinates of the tile where entity wants to attack.
     */
    protected void attack(Integer offsetX, Integer offsetY) {
        if (entityModel.getAlive()) {
            currRoom.attackGameObject(x + offsetX, y + offsetY, entityModel.getAttack());
        }
    }

    /**
     * Sends a request to the room to move the entity by a given offset.
     *
     * @param offsetX The X axis offset where the entity wants to move.
     * @param offsetY The Y axis offset where the entity wants to move.
     */
    public void move(Integer offsetX, Integer offsetY) {
        currRoom.moveEntity(this, offsetX, offsetY);
    }

    /**
     * Changes the attributes of it's model by a given offset.
     *
     * @param offsetMaxHealth   Offset by the entities maxHealth is to be changed.
     * @param offsetHealth      Offset by the entities health is to be changed.
     * @param offsetAttack      Offset by the entities attack is to be changed.
     * @param offsetAttackSpeed Offset by the entities attackSpeed/speed (for enemies) is to be changed.
     */
    public synchronized void changeStats(Integer offsetMaxHealth, Integer offsetHealth, Integer offsetAttack, Double offsetAttackSpeed, Integer offsetKeys) {
        Integer maxHealth = entityModel.getMaxHealth() + offsetMaxHealth;
        Integer health = entityModel.getHealth() + offsetHealth;
        health += offsetMaxHealth;
        Integer attack = entityModel.getAttack() + offsetAttack;
        Double attackSpeed = entityModel.getAttackSpeed() + offsetAttackSpeed;
        Integer keys = entityModel.getNumOfKeys() + offsetKeys;

        if (maxHealth <= 0) {    // Lower than 0 maxHealth case
            maxHealth = 1;
            health = 1;
        }
        if (health <= 0) {       // Death case
            health = 0;     // Edge case of negative health bar
            death();
        }
        if (attack <= 0) {       // Lower than 0 attack case
            attack = 1;
        }
        if (health > maxHealth) {// health bigger than maxHealth case
            health = maxHealth;
        }
        if (attackSpeed <= 0) {       // Lower than 0 attack case
            attackSpeed = 0.1D;
        }
        if (keys <= 0){
            keys = 0;
        }

        entityModel.setMaxHealth(maxHealth);
        entityModel.setHealth(health);
        entityModel.setAttack(attack);
        entityModel.setAttackSpeed(attackSpeed);
        entityModel.setNumOfKeys(keys);
    }

    /**
     * Deletes the entity from the room List of enemies. Called when entity has 0 health.
     */
    @Override
    public void death() {
        Logger.getLogger(getClass().getName()).log(Level.INFO, getClass().getName() + " has died.");
        entityModel.setAlive(false);
        if (currRoom != null) {
            currRoom.deleteEntity(this);
        } else {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Can't delete entity because currRoom has not been set!");
        }
    }

    public Integer[] getAttackingCoordsOffset() {
        return attackingCoordsOffset;
    }

    public Integer getHealth() {
        return entityModel.getHealth();
    }

    public Integer getMaxHealth() {
        return entityModel.getMaxHealth();
    }

    public Integer getAttack() {
        return entityModel.getAttack();
    }

    public Double getAttackSpeed() {
        return entityModel.getAttackSpeed();
    }

    public Boolean getAttacking() {
        return attacking;
    }

    public Integer getNumOfKeys() {
        return entityModel.getNumOfKeys();
    }

    public void setAttackingCoordsOffset(Integer x, Integer y) {
        attackingCoordsOffset[0] = x;
        attackingCoordsOffset[1] = y;
    }

    public void setCurrRoom(Room currRoom) {
        this.currRoom = currRoom;
    }

    public void setPlayer(PlayerController player) {
        this.player = player;
    }

}


