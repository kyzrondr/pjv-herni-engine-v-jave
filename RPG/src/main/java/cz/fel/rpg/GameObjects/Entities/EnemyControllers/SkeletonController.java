package cz.fel.rpg.GameObjects.Entities.EnemyControllers;

import cz.fel.rpg.GameObjects.Entities.EntityController;
import cz.fel.rpg.GameObjects.Entities.EntityModel;
import cz.fel.rpg.GameObjects.GameObject;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * An Enemy which goes after the player and attacks him. First matches the X coordinate of the player then the Y coordinate.
 */
public class SkeletonController extends EntityController {

    /**
     * A Skeleton blueprint that calls the EntityController blueprint.
     * @param health Amount of health the player has.
     * @param attack Amount of attack the player has.
     * @param speed Amount of attackSpeed the player has.
     */
    public SkeletonController(Integer health, Integer attack, Double speed) {
        super(health, attack, speed);
    }

    /**
     * Call the proper constructor of EntityController.
     * @param texture Path to the texture the GameObject uses.
     * @param x X coordinate of the GameObject in the room.
     * @param y Y coordinate of the GameObject in the room.
     * @param model A template model from which the health, attack and attackSpeed/speed.
     */
    private SkeletonController(String texture, Integer x, Integer y, EntityModel model) {
        super(texture, x, y, model);
    }

    /**
     * Does an action (movement or attack) while it's alive.
     */
    @Override
    public void run() {
        while (entityModel.getAlive()) {
            // If player is above attack him
            if (player.getX().equals(x) && player.getY().equals(y - 1)){
                attackingCoordsOffset[0] = 0;
                attackingCoordsOffset[1] = -1;
                attacking = true;
                attack(attackingCoordsOffset[0], attackingCoordsOffset[1]);
            }
            // if player is below attack him
            else if (player.getX().equals(x) && player.getY().equals(y + 1)){
                attackingCoordsOffset[0] = 0;
                attackingCoordsOffset[1] = 1;
                attacking = true;
                attack(attackingCoordsOffset[0], attackingCoordsOffset[1]);
            }
            // if player is left attack him
            else if (player.getX().equals(x - 1) && player.getY().equals(y)){
                attackingCoordsOffset[0] = -1;
                attackingCoordsOffset[1] = 0;
                attacking = true;
                attack(attackingCoordsOffset[0], attackingCoordsOffset[1]);
            }
            // if player is right attack him
            else if (player.getX().equals(x + 1) && player.getY().equals(y)){
                attackingCoordsOffset[0] = 1;
                attackingCoordsOffset[1] = 0;
                attacking = true;
                attack(attackingCoordsOffset[0], attackingCoordsOffset[1]);
            }
            // if player is more left than 1 tile go in left
            else if (player.getX() < x){
                move(-1, 0);
            }
            // if player is more right than 1 tile go in right
            else if (player.getX() > x){
                move(1, 0);
            }
            // if player is more up than 1 tile go in up
            else if (player.getY() < y){
                move(0, -1);
            }
            // if player is more down than 1 tile go in down
            else if (player.getY() > y){
                move(0, 1);
            }

            try {
                Thread.sleep((long) (SECONDS_IN_MS/entityModel.getAttackSpeed()));
            } catch (InterruptedException e) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Thread Interrupted error!");
                e.printStackTrace();
            }
            attacking = false;
        }
    }

    /**
     * @param texture Path to the texture the GameObject uses.
     * @param x X coordinate of the GameObject in the room.
     * @param y Y coordinate of the GameObject in the room.
     * @return Returns a properly created SkeletonController.
     */
    @Override
    public GameObject createGameObject(String texture, Integer x, Integer y) {
        return new SkeletonController(texture, x, y, entityModel);
    }
}
