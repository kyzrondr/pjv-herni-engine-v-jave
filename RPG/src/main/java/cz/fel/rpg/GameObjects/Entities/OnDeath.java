package cz.fel.rpg.GameObjects.Entities;

public interface OnDeath {

    /**
     * An action that's supposed to happen when an Entity dies.
     */
    void death();
}
