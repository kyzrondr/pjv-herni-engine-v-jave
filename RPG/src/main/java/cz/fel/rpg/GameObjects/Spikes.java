package cz.fel.rpg.GameObjects;

import cz.fel.rpg.GameObjects.Entities.EntityController;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A GameObject that blocks Entities and damages them upon Step On.
 */
public class Spikes extends GameObject implements StepOnAble {
    private final Integer damage;

    /**
     * A blueprint constructor for GameObjectIDs.
     *
     * @param damage The amount of damage the spikes deal to an entity that stepped on it.
     */
    public Spikes(Integer damage) {
        this.damage = damage;
    }

    /**
     * @param texture Path to the texture the GameObject uses.
     * @param x       X coordinate of the GameObject in the room.
     * @param y       Y coordinate of the GameObject in the room.
     * @param spikes  A Spikes object from which the damage is taken.
     */
    private Spikes(String texture, Integer x, Integer y, Spikes spikes) {
        super(texture, x, y);
        this.damage = spikes.damage;
    }

    /**
     * @param texture Path to the texture the GameObject uses.
     * @param x       X coordinate of the GameObject in the room.
     * @param y       Y coordinate of the GameObject in the room.
     * @return Returns a properly created Spikes.
     */
    @Override
    public GameObject createGameObject(String texture, Integer x, Integer y) {
        return new Spikes(texture, x, y, this);
    }

    /**
     * When the an EntityController steps on this it deals damage to it.
     *
     * @param entity The entity that stepped on this GameObject.
     */
    @Override
    public void steppedOn(EntityController entity) {
        Logger.getLogger(getClass().getName()).log(Level.INFO, "Damaged entity " + entity + ".");
        entity.changeStats(0, -damage, 0, 0D, 0);
    }

}
