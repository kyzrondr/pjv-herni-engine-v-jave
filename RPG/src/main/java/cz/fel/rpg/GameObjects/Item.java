package cz.fel.rpg.GameObjects;

import cz.fel.rpg.GameObjects.Entities.PlayerController;
import cz.fel.rpg.Level.Room;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A GameObject which holds the stats that are applied to the player upon pickup.
 */
public class Item extends GameObject implements Interactive {
    private final Integer maxHealth;
    private final Integer health;
    private final Integer attack;
    private final Double attackSpeed;
    private final Integer numOfKeys;

    /**
     * A blueprint constructor for GameObjectIDs.
     *
     * @param maxHealth   Offset of maxHealth that is applied to the player upon pickup.
     * @param health      Offset of health that is applied to the player upon pickup.
     * @param attack      Offset of attack that is applied to the player upon pickup.
     * @param attackSpeed Offset of attackSpeed that is applied to the player upon pickup.
     */
    public Item(Integer maxHealth, Integer health, Integer attack, Double attackSpeed, Integer numOfKeys) {
        this.maxHealth = maxHealth;
        this.health = health;
        this.attack = attack;
        this.attackSpeed = attackSpeed;
        this.numOfKeys = numOfKeys;
    }

    /**
     * Calls the GameObject constructor and creates a proper Item.
     *
     * @param texture Path to the texture the GameObject uses.
     * @param x       X coordinate of the GameObject in the room.
     * @param y       Y coordinate of the GameObject in the room.
     * @param item    A blueprint item from which the stats are extracted.
     */
    private Item(String texture, Integer x, Integer y, Item item) {
        super(texture, x, y);
        this.maxHealth = item.maxHealth;
        this.health = item.health;
        this.attack = item.attack;
        this.attackSpeed = item.attackSpeed;
        this.numOfKeys = item.numOfKeys;
    }

    /**
     * @param texture Path to the texture the GameObject uses.
     * @param x       X coordinate of the GameObject in the room.
     * @param y       Y coordinate of the GameObject in the room.
     * @return Returns a properly created Item.
     */
    @Override
    public GameObject createGameObject(String texture, Integer x, Integer y) {
        return new Item(texture, x, y, this);
    }

    /**
     * Changes the PlayerControllers stats based on it's values.
     *
     * @param room   The current room the Item is placed in.
     * @param player The PlayerController that interacted with the Item.
     */
    @Override
    public void interact(Room room, PlayerController player) {
        Logger.getLogger(getClass().getName()).log(Level.INFO, "Player picked up item with stats: " + this);
        player.changeStats(maxHealth, health, attack, attackSpeed, numOfKeys);
        room.placeGameObject(null, x, y);
    }

    @Override
    public String toString() {
        return "maxHealth +=" + maxHealth +
                ", health +=" + health +
                ", attack +=" + attack +
                ", attackSpeed +=" + attackSpeed;
    }
}
