package cz.fel.rpg.GameObjects;

/**
 * A GameObject that blocks Entities.
 */
public class Obstacle extends GameObject {

    /**
     * A blueprint constructor for GameObjectIDs.
     */
    public Obstacle() {
    }

    /**
     * Calls the GameObject constructor.
     *
     * @param texture Path to the texture the GameObject uses.
     * @param x       X coordinate of the GameObject in the room.
     * @param y       Y coordinate of the GameObject in the room.
     */
    private Obstacle(String texture, Integer x, Integer y) {
        super(texture, x, y);
    }

    /**
     * @param texture Path to the texture the GameObject uses.
     * @param x       X coordinate of the GameObject in the room.
     * @param y       Y coordinate of the GameObject in the room.
     * @return Returns a properly created Obstacle.
     */
    public GameObject createGameObject(String texture, Integer x, Integer y) {
        return new Obstacle(texture, x, y);
    }

}
