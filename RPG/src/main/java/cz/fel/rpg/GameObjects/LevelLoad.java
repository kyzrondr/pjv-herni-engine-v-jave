package cz.fel.rpg.GameObjects;

import cz.fel.rpg.GameObjects.Entities.EntityController;
import cz.fel.rpg.GameObjects.Entities.PlayerController;
import cz.fel.rpg.Utils.Utils;


/**
 * Upon stepping on this object it loads the last save.
 */
public class LevelLoad extends GameObject implements StepOnAble {

    /**
     * A blueprint constructor for GameObjectIDs.
     */
    public LevelLoad() {
    }

    /**
     * Calls the GameObject constructor.
     *
     * @param texture Path to the texture the GameObject uses.
     * @param x       X coordinate of the GameObject in the room.
     * @param y       Y coordinate of the GameObject in the room.
     */
    private LevelLoad(String texture, Integer x, Integer y) {
        super(texture, x, y);
    }


    /**
     * @param texture Path to the texture the GameObject uses.
     * @param x       X coordinate of the GameObject in the room.
     * @param y       Y coordinate of the GameObject in the room.
     * @return Returns a properly created LevelLoad.
     */
    @Override
    public GameObject createGameObject(String texture, Integer x, Integer y) {
        return new LevelLoad(texture, x, y);
    }

    /**
     * Calls the loadLevel util.
     *
     * @param entity The entity that stepped on the GameObject
     */
    @Override
    public void steppedOn(EntityController entity) {
        if (entity instanceof PlayerController) {
            PlayerController player = (PlayerController) entity;
            Utils.loadLevel(player.getCurrLevel());
        }
    }
}
