package cz.fel.rpg.GameObjects;

import cz.fel.rpg.GameObjects.Entities.PlayerController;
import cz.fel.rpg.Level.Room;

/**
 * A GameObject that can be unlocked with a key.
 */
public class Lock extends GameObject implements Interactive {

    /**
     * A blueprint constructor for GameObjectIDs.
     */
    public Lock() {
    }

    /**
     * Calls the GameObject constructor.
     *
     * @param texture Path to the texture the GameObject uses.
     * @param x       X coordinate of the GameObject in the room.
     * @param y       Y coordinate of the GameObject in the room.
     */
    private Lock(String texture, Integer x, Integer y) {
        super(texture, x, y);
    }


    /**
     * @param texture Path to the texture the GameObject uses.
     * @param x       X coordinate of the GameObject in the room.
     * @param y       Y coordinate of the GameObject in the room.
     * @return Returns a properly created Lock.
     */
    @Override
    public GameObject createGameObject(String texture, Integer x, Integer y) {
        return new Lock(texture, x, y);
    }

    /**
     * Destroys itself if the player has keys and deletes one from him.
     *
     * @param room   The room the GameObject is located in.
     * @param player The PlayerController that interacted with the GameObject.
     */
    @Override
    public void interact(Room room, PlayerController player) {
        if (player.getNumOfKeys() > 0) {
            player.changeStats(0, 0, 0, 0D, -1);
            room.placeGameObject(null, x, y);
        }
    }
}
