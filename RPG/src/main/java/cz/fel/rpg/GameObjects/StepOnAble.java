package cz.fel.rpg.GameObjects;

import cz.fel.rpg.GameObjects.Entities.EntityController;

public interface StepOnAble {

    /**
     * Do something when an entity steps on the GameObject that implements it.
     *
     * @param entity The entity that stepped on the GameObject
     */
    void steppedOn(EntityController entity);
}
