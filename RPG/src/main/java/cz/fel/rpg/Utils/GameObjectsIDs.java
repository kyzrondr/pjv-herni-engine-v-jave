package cz.fel.rpg.Utils;

import cz.fel.rpg.GameObjects.*;
import cz.fel.rpg.GameObjects.Entities.EnemyControllers.SkeletonController;
import cz.fel.rpg.GameObjects.Entities.EnemyControllers.SlimeController;
import cz.fel.rpg.GameObjects.Entities.PlayerController;

import java.util.HashMap;

/**
 * Holds the IDs of GameObjects and a HashMap with blueprints for the GameObjects.
 * To add a GameObject simply create a new public static final Integer ID and add it
 * to the HashMap below, with the corresponding GameObject blueprint.
 */
public class GameObjectsIDs {

    private GameObjectsIDs() {
    }

    public enum IDs {

        // Special Objects
        PLAYER_ID(1),
        LEVEL_EXIT_1_ID(2),
        GAME_EXIT_ID(3),
        CHEST_ID(4),
        LEVEL_SAVE(5),
        LEVEL_LOAD(6),

        // Obstacles
        OBSTACLE_1_ID(10), // ID used as the first obstacle
        OBSTACLE_INVISIBLE_ID(11),
        OBSTACLE_STUMP_ID(12),
        LOCK_ID(13),

        // Spikes
        SPIKES_ID(55), // ID used as the first spike
        SPIKES_THORNS_ID(56),

        // Enemies
        SLIME_CONTROLLER_ID(100), // ID used as the first enemy
        SKELETON_CONTROLLER_ID(101),

        // Items
        SECOND_HEART_ID(150), // ID used as the first item and last enemy bound!!
        RUSTY_GRIND_WHEEL(151),
        BIGGER_BICEPS(152),
        KEY(153),

        PLACEHOLDER_END_OF_ITEMS(200); // ID used to signify the end of the item pool!!


        public final int ID;

        IDs(int ID) {
            this.ID = ID;
        }
    }

    public static final HashMap<Integer, GameObject> mapIDs = getHashMapOfIDs();

    /**
     * @return A Hashmap of the blueprints for the GameObjects.
     */
    private static HashMap<Integer, GameObject> getHashMapOfIDs() {
        HashMap<Integer, GameObject> GameIDs = new HashMap<>();
        GameIDs.put(IDs.PLAYER_ID.ID, new PlayerController(100, 15, 1D));  // 1-9 are special objects such as
        GameIDs.put(IDs.LEVEL_EXIT_1_ID.ID, new LevelExit());                                   // the player, chests or level exits
        GameIDs.put(IDs.GAME_EXIT_ID.ID, new GameExit());
        GameIDs.put(IDs.CHEST_ID.ID, new Chest());
        GameIDs.put(IDs.LEVEL_SAVE.ID, new LevelSave());
        GameIDs.put(IDs.LEVEL_LOAD.ID, new LevelLoad());


        GameIDs.put(IDs.OBSTACLE_1_ID.ID, new Obstacle()); // 10-54 are Obstacles
        GameIDs.put(IDs.OBSTACLE_INVISIBLE_ID.ID, new Obstacle());
        GameIDs.put(IDs.OBSTACLE_STUMP_ID.ID, new Obstacle());
        GameIDs.put(IDs.LOCK_ID.ID, new Lock());

        GameIDs.put(IDs.SPIKES_ID.ID, new Spikes(10));  // 55-99 are Damaging obstacles called spikes
        GameIDs.put(IDs.SPIKES_THORNS_ID.ID, new Spikes(10));  // 55-99 are Damaging obstacles called spikes


        GameIDs.put(IDs.SLIME_CONTROLLER_ID.ID, new SlimeController(50, 0, 1D)); // 100-149 are Enemies
        GameIDs.put(IDs.SKELETON_CONTROLLER_ID.ID, new SkeletonController(70, 5, 1.2D));

        GameIDs.put(IDs.SECOND_HEART_ID.ID, new Item(25, 50, 0, 0D, 0));
        GameIDs.put(IDs.RUSTY_GRIND_WHEEL.ID, new Item(0, 0, 10, 0D, 0));
        GameIDs.put(IDs.BIGGER_BICEPS.ID, new Item(0, 0, 0, 0.4D, 0));
        GameIDs.put(IDs.KEY.ID, new Item(0, 0, 0, 0D, 1));


        return GameIDs;
    }
}
