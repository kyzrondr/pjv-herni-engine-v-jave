package cz.fel.rpg.Utils;

import cz.fel.rpg.GameObjects.Entities.PlayerController;
import cz.fel.rpg.GameObjects.Item;
import cz.fel.rpg.Level.LevelController;
import cz.fel.rpg.Level.LevelModel;
import cz.fel.rpg.Utils.GameObjectsIDs.*;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Holds the constants for the game such as Window size, capacity of rooms, path to textures and layouts, size of a tile etc.
 */
public class Utils {

    private Utils() {
    }

    public final static Integer WINDOW_WIDTH = 1280;
    public final static Integer WINDOW_HEIGHT = 759;    // So that the background can be 1280x720
    public final static Integer ROOM_OFFSET = -20;
    public final static Integer ROOM_Y_CAPACITY = 9;
    public final static Integer ROOM_X_CAPACITY = 13;
    public final static String GAME_NAME = "Modular Adventure";

    public static final String LEVEL_PATH = "classes/LevelsResources/";
    //public static final String LEVEL_PATH = "src/main/resources/LevelsResources/";
    public static final String MENU_UI_PATH = "classes/LevelsResources/MenuUI.png";
    //public static final String MENU_UI_PATH = "src/main/resources/LevelsResources/MenuUI.png";
    public static final String ATTACK_ICON_PATH = "classes/LevelsResources/AttackIcon.png";
    //public static final String ATTACK_ICON_PATH = "src/main/resources/LevelsResources/AttackIcon.png";
    public static final String HEALTH_BAR_PATH = "classes/LevelsResources/HealthBar.jpg";
    //public static final String HEALTH_BAR_PATH = "src/main/resources/LevelsResources/HealthBar.jpg";
    public static final String ROOM_PATH = "classes/Rooms/r";
    //public static final String ROOM_PATH = "src/main/resources/Rooms/r";
    public static final String OBJECT_PATH = "classes/GameObjectsResources/o";
    //public static final String OBJECT_PATH = "src/main/resources/GameObjectsResources/o";
    private static final String SAVE_PATH = "save.txt";

    public static final Integer START_ROOM_1 = 1;   // Room 1 is the game start room because it spawns the player and is the menu
    public static final Integer START_ROOM_2 = 2;   // Room 2 is a new level start room, so that the start can be anywhere in the level

    public static final Integer TILE_SIZE = 64;

    /**
     * Reads a txt file with Level alignment or Room alignment indexes (Can read both).
     *
     * @param filePath The path + name of the alignment file to be read.
     * @return A 2D array of integers representing a layout of a room or a level
     * @throws IOException If the file doesn't exist.
     */
    public static List<List<Integer>> readAlignment(String filePath) throws IOException {
        int i = 0, c;
        String num = "";
        FileReader fr = new FileReader(filePath);
        List<List<Integer>> roomAlignment = new ArrayList<>();
        roomAlignment.add(new ArrayList<>());

        while ((c = fr.read()) != -1) {
            if ((char) c == '\n') {
                roomAlignment.get(i).add(Integer.valueOf(num));
                num = "";
                i++;
                roomAlignment.add(new ArrayList<>());
            } else if ((char) c == ' ' && !num.equals("")) {
                roomAlignment.get(i).add(Integer.valueOf(num));
                num = "";
            } else if (Character.isDigit(c)) {
                num += (char) c;
            }
        }
        if (!num.equals("")) {
            roomAlignment.get(i).add(Integer.valueOf(num));
        }
        return roomAlignment;
    }

    /**
     * @param objectAlignment The alignment of GameObjects in a Room.
     * @throws IOException If the Room has the wrong size.
     */
    public static void checkObjectAlignmentSize(List<List<Integer>> objectAlignment) throws IOException {
        if (objectAlignment.size() != ROOM_Y_CAPACITY) {
            throw new IOException();
        }
        for (List<Integer> row : objectAlignment) {
            if (row.size() != ROOM_X_CAPACITY) {
                throw new IOException();
            }
        }
    }

    /**
     * SECOND_HEART_ID is used as the first item BEWARE!
     * PLACEHOLDER_END_OF_ITEMS is used as a bound of the last item (not an Item) BEWARE!
     *
     * @return An Item List of all known Items.
     */
    public static List<Item> getItems() {
        List<Item> items = new ArrayList<>();
        for (Integer key : GameObjectsIDs.mapIDs.keySet()) {
            if (key >= IDs.SECOND_HEART_ID.ID && key < IDs.PLACEHOLDER_END_OF_ITEMS.ID) {
                items.add((Item) GameObjectsIDs.mapIDs.get(key).createGameObject(OBJECT_PATH + key + ".png", 0, 0));
            }
        }
        return items;
    }

    /**
     * Loads a level and playerStats from the save.txt file
     *
     * @param player  The player that will be saved.
     * @param levelID The ID of the current level that will be saved.
     */
    public static void saveLevel(Integer levelID, PlayerController player) {
        Logger logger = Logger.getLogger("saveLevel");

        try {
            FileWriter fw = new FileWriter(SAVE_PATH);
            fw.write(levelID.toString() + " ");
            fw.write(player.getMaxHealth().toString() + " ");
            fw.write(player.getHealth().toString() + " ");
            fw.write(player.getAttack().toString() + " ");
            fw.write(player.getAttackSpeed().toString() + " ");
            fw.write(player.getNumOfKeys().toString() + " ");

            fw.flush();
            logger.log(Level.INFO, "Level Saved.");

        } catch (IOException e) {
            logger.log(Level.SEVERE, "Can't save, sorry.");
            e.printStackTrace();
        }
    }

    /**
     * Loads a level and playerStats from the save.txt file
     *
     * @param level The current levelController.
     */
    public static void loadLevel(LevelController level) {
        final int NUM_OF_ARGUMENTS = 6;

        Logger logger = Logger.getLogger("loadLevel");
        File file = new File(SAVE_PATH);
        try {
            Scanner sc = new Scanner(file);
            String[] line = sc.nextLine().split(" ");
            if (line.length == NUM_OF_ARGUMENTS) {
                Integer levelID = Integer.parseInt(line[0]) - 1;
                LevelModel lvlModel = new LevelModel(readAlignment(LEVEL_PATH + "l" + levelID + ".txt"), getItems(), level, levelID);
                level.setLvlModel(lvlModel);
                PlayerController player = level.getPlayer();
                player.changeStats(Integer.parseInt(line[1]) - player.getMaxHealth(), Integer.parseInt(line[2]) - player.getHealth(),
                        Integer.parseInt(line[3]) - player.getAttack(), Double.parseDouble(line[4]) - player.getAttackSpeed(),
                        Integer.parseInt(line[5]) - player.getNumOfKeys());
                level.setStayInLevel(false);
                logger.log(Level.INFO, "Level loaded.");
            } else {
                logger.log(Level.SEVERE, "Corrupted save, sorry.");
            }
        } catch (Exception e) {
            logger.log(Level.SEVERE, "Can't load save, it doesn't exist, sorry.");
        }

    }
}
