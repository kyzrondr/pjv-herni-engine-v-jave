package cz.fel.rpg.Utils;

import cz.fel.rpg.GameObjects.Entities.PlayerController;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Listens for input and calls move(), attack() and interact() on the player.
 * To prevent input overflow it accepts input only when no other keys are pressed.
 */
public class Input implements KeyListener {
    private final PlayerController player;
    private Boolean acceptInput = true;     // Used to only process input when no other keys are pressed

    /**
     * @param player The PlayerController which this Input moves, attack and interacts with.
     */
    public Input(PlayerController player) {
        this.player = player;
        Thread th = new Thread(player);
        th.start();
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    /**
     * If the player is alive and is not holding any other key it looks through the possible inputs and either moves,
     * attacks, interacts or does nothing with the player.
     *
     * @param e The key that has been pressed.
     */
    @Override
    public void keyPressed(KeyEvent e) {
        if (player.getAlive() && acceptInput) {
            if (e.getKeyCode() == KeyEvent.VK_W) {  // Move up
                player.move(0, -1);
                acceptInput = false;
            } else if (e.getKeyCode() == KeyEvent.VK_S) {  // Move down
                player.move(0, 1);
                acceptInput = false;
            } else if (e.getKeyCode() == KeyEvent.VK_A) {  // Move left
                player.move(-1, 0);
                acceptInput = false;
            } else if (e.getKeyCode() == KeyEvent.VK_D) {  // Move right
                player.move(1, 0);
                acceptInput = false;
            } else if (!player.getAttacking() && e.getKeyCode() == KeyEvent.VK_UP) {    // Attack up
                player.setAttackingCoordsOffset(0, -1);
                acceptInput = false;
            } else if (!player.getAttacking() && e.getKeyCode() == KeyEvent.VK_DOWN) {  // Attack down
                player.setAttackingCoordsOffset(0, 1);
                acceptInput = false;
            } else if (!player.getAttacking() && e.getKeyCode() == KeyEvent.VK_LEFT) {  // Attack left
                player.setAttackingCoordsOffset(-1, 0);
                acceptInput = false;
            } else if (!player.getAttacking() && e.getKeyCode() == KeyEvent.VK_RIGHT) {     // Attack right
                player.setAttackingCoordsOffset(1, 0);
                acceptInput = false;
            } else if (e.getKeyCode() == KeyEvent.VK_E) {   // Interact
                player.interactWithSurrounding();
                acceptInput = false;
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        acceptInput = true;
    }
}
