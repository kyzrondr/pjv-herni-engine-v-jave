package cz.fel.rpg;

import cz.fel.rpg.Level.LevelController;
import cz.fel.rpg.Level.LevelModel;
import cz.fel.rpg.Level.LevelView;

import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static cz.fel.rpg.Utils.Utils.*;


public class MainGameLoop {

    /**
     * Reads a level from a file, initializes everything and starts the level.
     *
     * @param lvlController A LevelController that has all the logic inside it. (LvlView must be set.)
     * @param levelID       ID of the level to be started.
     * @throws IOException When the next level doesn't exist.
     */
    public static Integer startLevel(LevelController lvlController, Integer levelID) throws IOException {
        LevelModel lvlModel = new LevelModel(readAlignment(LEVEL_PATH + "l" + levelID + ".txt"), getItems(), lvlController, levelID);

        if (lvlModel.getRoomsAlignment().get(0).isEmpty()) {
            Logger.getLogger(MainGameLoop.class.getName()).log(Level.SEVERE, "Empty level continuing to level " + (levelID + 1));
        } else {
            lvlController.setLvlModel(lvlModel);
            lvlController.initLevel();
        }

        return lvlModel.getLevelID();
    }

    /**
     * Runs the main game loop.
     *
     * @param args Program arguments (not currently used).
     */
    public static void main(String[] args) {
        int i = 0;
        LevelController lvlController = new LevelController();
        LevelView lvlView = new LevelView(i);
        lvlView.setLvlController(lvlController);
        lvlController.setLvlView(lvlView);
        lvlView.initView();
        System.out.println("Working Directory = " + System.getProperty("user.dir"));
        while (lvlController.getStayInGame()) {
            try {
                i = startLevel(lvlController, i);   // When player dies it resets him to the menu
                if (!lvlController.getPlayer().getAlive()) {
                    i = -1;
                }
            } catch (IOException e) {   // When there are no more levels the Player wins and is sent back to the menu
                Logger.getLogger(MainGameLoop.class.getName()).log(Level.WARNING, "No more levels available!");
                lvlView.drawVictoryScreen();
                i = -1;
            }
            i++;
        }
        lvlView.dispatchEvent(new WindowEvent(lvlView, WindowEvent.WINDOW_CLOSING));
    }
}
