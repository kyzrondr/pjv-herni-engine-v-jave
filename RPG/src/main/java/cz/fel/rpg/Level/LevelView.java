package cz.fel.rpg.Level;

import cz.fel.rpg.GameObjects.Entities.EntityController;
import cz.fel.rpg.GameObjects.Entities.PlayerController;
import cz.fel.rpg.GameObjects.GameObject;
import cz.fel.rpg.GameObjects.LevelExit;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import static cz.fel.rpg.Utils.Utils.*;


/**
 * The main JFrame where everything is rendered.
 */
public class LevelView extends JFrame {
    private BufferedImage background;
    private final ImageIcon healthBar;
    private final ImageIcon attackIcon;
    private final ImageIcon doorIcon;
    private final ImageIcon menuUI;
    private LevelController lvlController;
    private Integer levelID;
    private Canvas canvas;

    /**
     * In case of Invalid texturePaths it places nothing there.
     *
     * @param levelID The ID of the current level.
     */
    public LevelView(Integer levelID) {
        super(GAME_NAME);
        this.levelID = levelID;
        this.healthBar = new ImageIcon(HEALTH_BAR_PATH);
        this.attackIcon = new ImageIcon(ATTACK_ICON_PATH);
        this.doorIcon = new ImageIcon(LEVEL_PATH + "l" + levelID + "Door.png");
        this.menuUI = new ImageIcon(MENU_UI_PATH);
        this.background = setBackground();
    }

    /**
     * Called upon initialization and upon changing the levelID.
     *
     * @return A background for the current scene.
     */
    private BufferedImage setBackground() {
        try {
            return ImageIO.read(new File(LEVEL_PATH + "l" + levelID + "Back.jpg"));
        } catch (IOException e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "No level background found, setting default color.");
            BufferedImage bi = new BufferedImage(WINDOW_WIDTH, WINDOW_HEIGHT, BufferedImage.TYPE_INT_RGB);
            bi.getGraphics().setColor(Color.black);
            return bi;
        }
    }

    /**
     * Main renderer of the game. Renders GameOver screen, the room GameObjects, menu GUI and player stats.
     */
    public void viewCurrRoom() {
        // Constants for adjusting the GUI position
        int MENU_Y_OFFSET = -24;

        Room room = lvlController.getCurrRoom();
        BufferedImage backgroundCopy = new BufferedImage(background.getWidth(), background.getHeight(), background.getType());
        backgroundCopy.setData(background.getData());
        PlayerController player = lvlController.getPlayer();

        // JFrame coordinates of the first object in a room
        int objectX = (getWidth() - (room.getRoomObjects().get(0).size() * TILE_SIZE)) / 2 + 2 * TILE_SIZE;
        int objectY = (getHeight() - (room.getRoomObjects().size() * TILE_SIZE)) / 2 + ROOM_OFFSET;

        Graphics2D g = backgroundCopy.createGraphics();

        // Draws GameOver screen when player is dead
        if (!player.getAlive()) {
            drawGameOverScreen(g);
        } else {

            // Draws the menu UI if player is in room 1 (menu)
            if (room.getRoomID().equals(START_ROOM_1)) {
                g.drawImage(menuUI.getImage(), 0, MENU_Y_OFFSET, getWidth(), getHeight(), null);
            }

            // Draws the GameObjects
            drawGameObjects(g, room, objectX, objectY);

            // Draws the room exits/doors if there are no enemies
            if (room.getEnemiesSize() == 0) {
                drawDoors(room, g, objectX, objectY);
            }

            // Draws the players stats
            drawPlayerStats(g, player);
        }

        canvas.getGraphics().drawImage(backgroundCopy, 0, 0, null);
    }

    /**
     * Draws the GameObjects from a given room to the background graphics (g).
     *
     * @param g       Graphics of the background on which the GameObjects are drawn.
     * @param room    The room from which the GameObjects are taken to be drawn.
     * @param objectX The X coordinates of the first tile in the room grid.
     * @param objectY The Y coordinates of the first tile in the room grid.
     */
    private void drawGameObjects(Graphics2D g, Room room, Integer objectX, Integer objectY) {
        for (List<GameObject> list : room.getRoomObjects()) {
            for (GameObject gameObject : list) {
                // If there are no enemies it draws a LevelExit if present
                Optional<GameObject> opGameObject = Optional.ofNullable(gameObject);
                if (opGameObject.isPresent() && (!(opGameObject.get() instanceof LevelExit) || room.getEnemiesSize() == 0)) {
                    g.drawImage(opGameObject.get().getTexture().getImage(), objectX + opGameObject.get().getX() * TILE_SIZE
                            , objectY + opGameObject.get().getY() * TILE_SIZE, TILE_SIZE, TILE_SIZE, null);
                    if (opGameObject.get() instanceof EntityController && ((EntityController) opGameObject.get()).getAttacking()) {
                        drawAttackIcon((EntityController) opGameObject.get(), g, objectX, objectY);
                    }
                }
            }
        }
    }

    /**
     * Draws an AttackIcon next to the enemy based on the direction of an attack.
     *
     * @param entity  The entity which is attacking.
     * @param g       Graphics of the background on which the AttackIcon is drawn.
     * @param objectX The X coordinates of the first tile in the room grid.
     * @param objectY The Y coordinates of the first tile in the room grid.
     */
    private void drawAttackIcon(EntityController entity, Graphics2D g, Integer objectX, Integer objectY) {

        // Constants for adjusting the attack icon position
        int ICON_UP_X = 20;
        int ICON_UP_Y = -15;
        int ICON_RIGHT_X = 48;
        int ICON_RIGHT_Y = 0;
        int ICON_DOWN_X = 32;
        int ICON_DOWN_Y = 48;
        int ICON_LEFT_X = 5;
        int ICON_LEFT_Y = 0;

        Integer[] attackCoords = entity.getAttackingCoordsOffset();
        Integer iconX;
        Integer iconY;

        // Attack up
        if (attackCoords[0] == 0 && attackCoords[1] == -1) {
            iconX = objectX + entity.getX() * TILE_SIZE + ICON_UP_X;
            iconY = objectY + ICON_UP_Y + entity.getY() * TILE_SIZE;
            g.drawImage(attackIcon.getImage(), iconX, iconY, attackIcon.getIconWidth() * 2, attackIcon.getIconHeight() * 2, null);
        }
        // Attack right
        else if (attackCoords[0] == 1 && attackCoords[1] == 0) {
            iconX = objectX + ICON_RIGHT_X + entity.getX() * TILE_SIZE;
            iconY = objectY + ICON_RIGHT_Y + entity.getY() * TILE_SIZE;
            g.drawImage(attackIcon.getImage(), iconX, iconY, attackIcon.getIconWidth() * 2, attackIcon.getIconHeight() * 2, null);
        }
        // Attack down
        else if (attackCoords[0] == 0 && attackCoords[1] == 1) {
            iconX = objectX + ICON_DOWN_X + attackIcon.getIconWidth() + entity.getX() * TILE_SIZE;
            iconY = objectY + ICON_DOWN_Y + entity.getY() * TILE_SIZE;
            g.drawImage(attackIcon.getImage(), iconX, iconY, -(attackIcon.getIconWidth() * 2), attackIcon.getIconHeight() * 2, null);
        }
        // Attack left
        else if (attackCoords[0] == -1 && attackCoords[1] == 0) {
            iconX = objectX + ICON_LEFT_X + attackIcon.getIconWidth() + entity.getX() * TILE_SIZE;
            iconY = objectY + ICON_LEFT_Y + entity.getY() * TILE_SIZE;
            g.drawImage(attackIcon.getImage(), iconX, iconY, -(attackIcon.getIconWidth() * 2), attackIcon.getIconHeight() * 2, null);
        }
    }

    /**
     * Draws rotated doors to the adjacent rooms of the given room. Called when all enemies have died.
     *
     * @param room    The room from which the possible exits are taken and drawn.
     * @param g       Graphics of the background on which the doors are drawn.
     * @param objectX The X coordinates of the first tile in the room grid.
     * @param objectY The Y coordinates of the first tile in the room grid.
     */
    private void drawDoors(Room room, Graphics2D g, Integer objectX, Integer objectY) {
        BufferedImage doorBi = new BufferedImage(TILE_SIZE, TILE_SIZE, BufferedImage.TYPE_INT_RGB);
        doorBi.getGraphics().drawImage(doorIcon.getImage(), 0, 0, TILE_SIZE, TILE_SIZE, null);

        AffineTransform af = new AffineTransform();
        af.rotate(Math.PI / 2, TILE_SIZE * 0.5, TILE_SIZE * 0.5);
        AffineTransformOp op = new AffineTransformOp(af, AffineTransformOp.TYPE_BILINEAR);

        // Door above
        if (room.getCanExitUp()) {
            g.drawImage(doorBi, objectX + TILE_SIZE * (room.getWidth() / 2), objectY - TILE_SIZE, TILE_SIZE, TILE_SIZE, null);
        }
        // Door right
        doorBi = op.filter(doorBi, null);
        if (room.getCanExitRight()) {
            g.drawImage(doorBi, objectX + TILE_SIZE * room.getWidth(), objectY + TILE_SIZE * (room.getHeight() / 2), TILE_SIZE, TILE_SIZE, null);
        }
        // Door left
        doorBi = op.filter(doorBi, null);
        if (room.getCanExitDown()) {
            g.drawImage(doorBi, objectX + TILE_SIZE * (room.getWidth() / 2), objectY + TILE_SIZE * room.getHeight(), TILE_SIZE, TILE_SIZE, null);
        }
        // Door below
        doorBi = op.filter(doorBi, null);
        if (room.getCanExitLeft()) {
            g.drawImage(doorBi, objectX - TILE_SIZE, objectY + TILE_SIZE * (room.getHeight() / 2), TILE_SIZE, TILE_SIZE, null);
        }
    }

    /**
     * Draws a GameOver screen before exiting to the menu.
     *
     * @param g Graphics of the background on which the GameOver screen is drawn.
     */
    private void drawGameOverScreen(Graphics g) {
        // Constants for adjusting the GUI position
        float GAME_OVER_FONT_SIZE = 4f;
        int GAME_OVER_X_OFFSET = 120;
        int GAME_OVER_Y_OFFSET = 350;
        float BACK_TO_MENU_FONT_SIZE = 3.5f;
        int BACK_TO_MENU_X_OFFSET = 320;
        int BACK_TO_MENU_Y_OFFSET = 430;

        g.setColor(Color.white);
        g.clearRect(0, 0, getWidth(), getHeight());
        g.setFont(g.getFont().deriveFont(g.getFont().getSize() * BACK_TO_MENU_FONT_SIZE));
        g.drawString("Sending player back to menu...", BACK_TO_MENU_X_OFFSET, BACK_TO_MENU_Y_OFFSET);
        g.setFont(g.getFont().deriveFont(g.getFont().getSize() * GAME_OVER_FONT_SIZE));
        g.drawString("GAME OVER", GAME_OVER_X_OFFSET, GAME_OVER_Y_OFFSET);
    }

    /**
     * Draws the players stats on the background.
     *
     * @param g      Graphics of the background on which the Players stats is drawn.
     * @param player The player which stats will be drawn.
     */
    private void drawPlayerStats(Graphics g, PlayerController player) {
        // Constants for adjusting the GUI position
        float FONT_SIZE = 3.5f;
        int HEALTH_X_OFFSET = 68;
        int HEALTH_Y_OFFSET = 170;
        int ATTACK_X_OFFSET = 185;
        int ATTACK_Y_OFFSET = 290;
        int SPEED_X_OFFSET = 180;
        int SPEED_Y_OFFSET = 350;
        int HEALTH_BAR_X_OFFSET = 47;
        int HEALTH_BAR_Y_OFFSET = 187;

        g.setFont(g.getFont().deriveFont(g.getFont().getSize() * FONT_SIZE));
        g.setColor(Color.black);
        g.drawString(lvlController.getPlayer().getHealth() + "/" + lvlController.getPlayer().getMaxHealth(), HEALTH_X_OFFSET, HEALTH_Y_OFFSET);
        g.drawString(lvlController.getPlayer().getAttack().toString(), ATTACK_X_OFFSET, ATTACK_Y_OFFSET);
        g.drawString(String.valueOf((Math.round(lvlController.getPlayer().getAttackSpeed() * 10) / 10f)), SPEED_X_OFFSET, SPEED_Y_OFFSET);
        g.drawImage(healthBar.getImage(), HEALTH_BAR_X_OFFSET, HEALTH_BAR_Y_OFFSET, Math.round(((float) healthBar.getIconWidth() / player.getMaxHealth()) * player.getHealth())
                , healthBar.getIconHeight(), null);
    }

    /**
     * Draws a Victory screen before exiting to the menu.
     */
    public void drawVictoryScreen() {
        // Constants for adjusting the attack icon position
        float BACK_TO_MENU_FONT_SIZE = 3.5f;
        int BACK_TO_MENU_X_OFFSET = 320;
        int BACK_TO_MENU_Y_OFFSET = 430;
        float VICTORY_FONT_SIZE = 4f;
        int VICTORY_X_OFFSET = 260;
        int VICTORY_Y_OFFSET = 350;

        Graphics g = canvas.getGraphics();
        g.clearRect(0, 0, getWidth(), getHeight());
        g.setColor(Color.white);
        g.setFont(g.getFont().deriveFont(g.getFont().getSize() * BACK_TO_MENU_FONT_SIZE));
        g.drawString("Sending player back to menu...", BACK_TO_MENU_X_OFFSET, BACK_TO_MENU_Y_OFFSET);
        g.setFont(g.getFont().deriveFont(g.getFont().getSize() * VICTORY_FONT_SIZE));
        g.drawString("You won!", VICTORY_X_OFFSET, VICTORY_Y_OFFSET);
        lvlController.getPlayer().changeStats(0, -lvlController.getPlayer().getHealth(), 0, 0D, 0);
    }

    /**
     * Creates a new Canvas on which everything is drawn. Sets the window size,
     * default close operation, visible to true and resizable to false.
     */
    public void initView() {
        canvas = new Canvas();
        canvas.setBackground(Color.black);
        add(canvas);
        this.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        this.setResizable(false);
        this.setVisible(true);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public void setLevelID(Integer levelID) {
        this.levelID = levelID;
        this.background = setBackground();
    }

    public void setLvlController(LevelController lvlController) {
        this.lvlController = lvlController;
    }

    public Canvas getCanvas() {
        return canvas;
    }
}
