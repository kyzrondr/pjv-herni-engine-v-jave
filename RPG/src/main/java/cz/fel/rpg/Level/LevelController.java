package cz.fel.rpg.Level;

import cz.fel.rpg.GameObjects.Chest;
import cz.fel.rpg.GameObjects.Entities.EntityController;
import cz.fel.rpg.GameObjects.Entities.PlayerController;
import cz.fel.rpg.GameObjects.GameObject;
import cz.fel.rpg.Utils.GameObjectsIDs;
import cz.fel.rpg.Utils.GameObjectsIDs.*;
import cz.fel.rpg.Utils.Input;
import cz.fel.rpg.Utils.Utils;

import java.awt.event.KeyListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import static cz.fel.rpg.Utils.Utils.*;

/**
 * The logic of a Level. It starts the level, creates rooms, changes rooms, initializes entities and Input for the player.
 */
public class LevelController {
    private LevelModel lvlModel;
    private LevelView lvlView;
    private Room currRoom;
    private Integer currRoomX;
    private Integer currRoomY;
    private PlayerController player;
    private Boolean stayInLevel = true;
    private Boolean stayInGame = true;

    public LevelController() {
    }

    /**
     * Creates a new instance of Input class and adds as a KeyListener to the Canvas of the current LevelView.
     */
    private void initInput() {
        lvlView.getCanvas().addKeyListener(new Input(player));
    }

    /**
     * Initializes a Level by creating a 2D array for initialized rooms, finding the starting room (if not found the
     * first room is the starting room), initializing it and placing the player in it. Then it starts the level.
     */
    public void initLevel() {
        Logger.getLogger(getClass().getName()).log(Level.INFO, "Initializing the level...");

        stayInLevel = true;
        stayInGame = true;

        int columnCapacity = lvlModel.getRoomsAlignment().size();
        int rowCapacity = lvlModel.getRoomsAlignment().get(0).size();
        Integer startX = null, startY = null;
        Integer backupStartX = null, backupStartY = null;   // Used when level doesn't have a starting room (ID 1 or 2)

        List<List<Room>> initRooms = new ArrayList<>();
        for (int y = 0; y < columnCapacity; y++) {
            initRooms.add(new ArrayList<>());
            for (int x = 0; x < rowCapacity; x++) {
                initRooms.get(y).add(null);
                if (lvlModel.getRoomsAlignment().get(y).get(x).equals(START_ROOM_1) ||
                        lvlModel.getRoomsAlignment().get(y).get(x).equals(START_ROOM_2)) {
                    startX = x;
                    startY = y;
                } else if (startX == null && backupStartX == null && lvlModel.getRoomsAlignment().get(y).get(x) != null) {
                    backupStartX = x;
                    backupStartY = y;
                }
            }
        }

        if (startX == null) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Level doesn't have a start room (room 1 or room 2)! Putting the player in first possible room.");
            lvlModel.setStartX(backupStartX);
            lvlModel.setStartY(backupStartY);
        } else {
            lvlModel.setStartX(startX);
            lvlModel.setStartY(startY);
        }

        lvlModel.setInitializedRooms(initRooms);
        Integer roomID = lvlModel.getRoomsAlignment().get(lvlModel.getStartY()).get(lvlModel.getStartX());
        initRoom(roomID, lvlModel.getStartX(), lvlModel.getStartY());
        player.setCurrLevel(this);
        currRoom.placeGameObject(getPlayer(), getPlayer().getX(), getPlayer().getY());

        startLevel();
    }


    /**
     * Calls the Level renderer while the player is in the level. When level ends it centers the player
     */
    private void startLevel() {
        Logger.getLogger(getClass().getName()).log(Level.INFO, "Started the level.");
        while (stayInLevel) {
            lvlView.viewCurrRoom();
        }

        KeyListener[] keyListeners = lvlView.getCanvas().getKeyListeners();
        if (!getPlayer().getAlive() && keyListeners.length > 0) {
            lvlView.getCanvas().removeKeyListener(keyListeners[0]);
        }

        centerPlayer();
    }

    /**
     * Sets the players coordinates to the middle of the room
     */
    private void centerPlayer() {
        player.setX(currRoom.getWidth() / 2);
        player.setY(currRoom.getHeight() / 2);
    }

    /**
     * Reads a room alignment (how are gameObjects placed in the room) and creates the room.
     * In case of non-valid room layout or non-existent room it creates an empty room (ID = 2)
     *
     * @param roomID The ID of the room that's supposed to be initialized.
     * @param roomX  The X coordinate of the room in the level.
     * @param roomY  The Y coordinate of the room in the level.
     */
    public void initRoom(Integer roomID, Integer roomX, Integer roomY) {
        List<List<Integer>> objectAlignment;
        try {
            objectAlignment = Utils.readAlignment(ROOM_PATH + roomID + ".txt");
            checkObjectAlignmentSize(objectAlignment);
        } catch (IOException e) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Room " + roomID + " doesn't exist or has the wrong layout! Loading room 2...");
            roomID = START_ROOM_2;
            objectAlignment = new ArrayList<>(Collections.nCopies(ROOM_Y_CAPACITY, new ArrayList<>(Collections.nCopies(ROOM_X_CAPACITY, 0))));
        }

        Logger.getLogger(getClass().getName()).log(Level.INFO, "Initializing new room " + roomID);

        currRoom = createRoom(objectAlignment, roomID);
        currRoom.initEntities(player);
        currRoom.setExits(lvlModel.getRoomsAlignment(), roomX, roomY);
        currRoomX = roomX;
        currRoomY = roomY;
        lvlModel.getInitializedRooms().get(roomY).set(roomX, currRoom);
    }

    /**
     * Creates a Room and the GameObjects in it. In case of non-existent GameObject it puts nothing there (null).
     * When a player is in the room and no player has yet been created or the current one died it creates a new Input.
     *
     * @param objectAlignment The alignment of GameObjects in the room.
     * @param roomID          ID of the room that's supposed to be created
     * @return A new Room with initialized objects. (entities not initialized)
     */
    private Room createRoom(List<List<Integer>> objectAlignment, Integer roomID) {
        List<List<GameObject>> roomObjects = new ArrayList<>();
        List<EntityController> enemies = new ArrayList<>();
        Integer ID;
        GameObject gameObject;

        for (int y = 0; y < objectAlignment.size(); y++) {
            roomObjects.add(new ArrayList<>());
            for (int x = 0; x < objectAlignment.get(y).size(); x++) {
                ID = objectAlignment.get(y).get(x);
                if (GameObjectsIDs.mapIDs.containsKey(ID)) {
                    gameObject = GameObjectsIDs.mapIDs.get(ID).createGameObject(OBJECT_PATH + ID + ".png", x, y);
                    if (ID.equals(IDs.PLAYER_ID.ID)) { // Only one player allowed
                        if (getPlayer() == null || !getPlayer().getAlive()) {
                            player = (PlayerController) gameObject;
                            initInput();
                        } else {
                            gameObject = null;
                        }
                    } else if (ID.equals(IDs.CHEST_ID.ID)) {
                        ((Chest) gameObject).setItem(lvlModel.getItems().get((new Random()).nextInt(lvlModel.getItems().size())));
                    } else if (ID >= IDs.SLIME_CONTROLLER_ID.ID && ID < IDs.SECOND_HEART_ID.ID) {
                        enemies.add((EntityController) gameObject);
                    }
                } else {
                    if (ID != 0) {
                        Logger.getLogger(getClass().getName()).log(Level.WARNING, "GameObject ID " + ID + " is not known. " +
                                "Please add it to GameObjectIDs. Putting null instead.");
                    }
                    gameObject = null;
                }
                roomObjects.get(y).add(gameObject);
            }
        }

        return new Room(roomObjects, enemies, this, roomID);
    }

    /**
     * Changes the current room to the next one based on the X and Y coordinates offset. If the next room has already
     * been initialized (not room ID but the exact room) it loads it -> after killing enemies they don't respawn when
     * coming back to the room.
     *
     * @param offsetX The X coordinate offset of the next room.
     * @param offsetY The Y coordinate offset of the next room.
     */
    public void changeRoom(Integer offsetX, Integer offsetY) {
        List<List<Integer>> rooms = lvlModel.getRoomsAlignment();

        if (currRoomX + offsetX >= 0 && currRoomY + offsetY >= 0 &&     // Checks if new room index is in bound
                currRoomX + offsetX < rooms.get(currRoomY).size() && currRoomY + offsetY < rooms.size()) {

            Integer roomID = rooms.get(currRoomY + offsetY).get(currRoomX + offsetX);
            Logger.getLogger(getClass().getName()).log(Level.INFO, "Changing current room to room " + roomID);

            // Checks initialized rooms if the room is there
            if (lvlModel.getInitializedRooms().get(currRoomY + offsetY).get(currRoomX + offsetX) == null) {
                initRoom(roomID, currRoomX + offsetX, currRoomY + offsetY);

            } else {        // Creates a new one
                currRoomX += offsetX;
                currRoomY += offsetY;
                currRoom = lvlModel.getInitializedRooms().get(currRoomY).get(currRoomX);
                player.setCurrRoom(currRoom);
            }

            if (offsetX == 0 && offsetY == -1) {
                player.setY(currRoom.getHeight() - 1);
            } else if (offsetX == 0 && offsetY == 1) {
                player.setY(0);
            } else if (offsetX == -1 && offsetY == 0) {
                player.setX(currRoom.getWidth() - 1);
            } else if (offsetX == 1 && offsetY == 0) {
                player.setX(0);
            }
            currRoom.placeGameObject(player, player.getX(), player.getY());
        }
    }

    public Room getCurrRoom() {
        return currRoom;
    }

    public PlayerController getPlayer() {
        return player;
    }

    public Boolean getStayInGame() {
        return stayInGame;
    }

    public Boolean getStayInLevel() {
        return stayInLevel;
    }

    public Integer getLevelID(){
        return lvlModel.getLevelID();
    }

    public void setLvlModel(LevelModel lvlModel) {
        this.lvlModel = lvlModel;
        lvlView.setLevelID(lvlModel.getLevelID());
    }

    public void setLvlView(LevelView lvlView) {
        this.lvlView = lvlView;
    }

    public void setStayInLevel(Boolean stayInLevel) {
        this.stayInLevel = stayInLevel;
    }

    public void setStayInGame(Boolean stayInGame) {
        this.stayInGame = stayInGame;
    }
}
