package cz.fel.rpg.Level;

import cz.fel.rpg.GameObjects.Entities.EntityController;
import cz.fel.rpg.GameObjects.Entities.PlayerController;
import cz.fel.rpg.GameObjects.GameObject;
import cz.fel.rpg.GameObjects.Interactive;
import cz.fel.rpg.GameObjects.LevelExit;
import cz.fel.rpg.GameObjects.StepOnAble;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * A class which holds the GameObjects present in a room, handles the movement and attack of enemies and interactions of the player.
 */
public class Room {
    private final List<List<GameObject>> roomObjects;
    private final LevelController levelController;
    private final List<EntityController> enemies;
    private final Integer roomID;
    private Boolean canExitLeft;
    private Boolean canExitRight;
    private Boolean canExitUp;
    private Boolean canExitDown;

    /**
     * @param roomObjects     A 2D array of GameObjects that are in this room.
     * @param enemies         The enemies that are present in the room.
     * @param levelController The current LevelController. Used for exiting the room.
     * @param roomID          The ID of the current room.
     */
    public Room(List<List<GameObject>> roomObjects, List<EntityController> enemies, LevelController levelController, Integer roomID) {
        this.roomObjects = roomObjects;
        this.enemies = enemies;
        this.levelController = levelController;
        this.roomID = roomID;
        this.canExitLeft = false;
        this.canExitRight = false;
        this.canExitUp = false;
        this.canExitDown = false;
    }

    /**
     * Sets the current room to the player and for all enemies then it starts a Thread for each enemy.
     *
     * @param player The PlayerController located in the room.
     */
    public void initEntities(PlayerController player) {
        if (player == null) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "Player doesn't exist please add it to the first level. Ending game.");
            System.exit(-1);
        }
        player.setCurrRoom(this);

        for (EntityController entity : enemies) {
            entity.setCurrRoom(this);
            entity.setPlayer(player);
            Thread th = new Thread(entity);
            th.start();
        }
    }

    /**
     * Moves a entity in the room if the tile where it wants to go is empty.
     * If the object is StepOnAble it calls the steppedOn() function of the GameObject.
     * If the Entity is the Player, there are no more enemies and the player wants to move to a roomExit (door)
     * it calls moveToNewRoom()
     *
     * @param entity  The entity that wants to move.
     * @param offsetX The offset x coordinate where the entity wants to move. (direction)
     * @param offsetY The offset y coordinate where the entity wants to move. (direction)
     */
    public synchronized void moveEntity(EntityController entity, Integer offsetX, Integer offsetY) {
        Integer currX = entity.getX();
        Integer currY = entity.getY();

        if (isXYinBounds(currX + offsetX, currY + offsetY)) {
            GameObject object = roomObjects.get(currY + offsetY).get(currX + offsetX);
            if (object == null) {
                placeGameObject(null, currX, currY);
                placeGameObject(entity, offsetX + currX, offsetY + currY);
            }
            // Enables a StepOnAble object. If it's a LevelExit it functions only if there are no enemies.
            else if (object instanceof StepOnAble && (!(object instanceof LevelExit) || enemies.size() == 0)) {
                ((StepOnAble) object).steppedOn(entity);
            }
        } else if (enemies.size() == 0 && entity instanceof PlayerController) {
            moveToNewRoom(currX, offsetX, currY, offsetY);
        }
    }

    /**
     * Moves the player to a new room.
     *
     * @param currX   The current x coordinate of the player.
     * @param offsetX The offset x coordinate where the player wants to move. (direction)
     * @param currY   The current y coordinate of the player.
     * @param offsetY The offset y coordinate where the player wants to move. (direction)
     */
    private void moveToNewRoom(Integer currX, Integer offsetX, Integer currY, Integer offsetY) {
        // Move a room up
        if (canExitUp && currY + offsetY == -1 && currX + offsetX == roomObjects.get(currY).size() / 2) {
            roomObjects.get(currY).set(currX, null);
            levelController.changeRoom(0, -1);
        }
        // Move a room down
        else if (canExitDown && currY + offsetY == roomObjects.size() && currX + offsetX == roomObjects.get(currY).size() / 2) {
            roomObjects.get(currY).set(currX, null);
            levelController.changeRoom(0, 1);
        }
        // Move a room right
        else if (canExitRight && currY + offsetY == roomObjects.size() / 2 && currX + offsetX == roomObjects.get(currY).size()) {
            roomObjects.get(currY).set(currX, null);
            levelController.changeRoom(1, 0);
        }
        // Move a room left
        else if (canExitLeft && currY + offsetY == roomObjects.size() / 2 && currX + offsetX == -1) {
            roomObjects.get(currY).set(currX, null);
            levelController.changeRoom(-1, 0);
        }
    }

    /**
     * Removes an EntityControlled from the room and room.enemies List. (Not the player)
     *
     * @param entity The entity to be removed from enemies in the current room.
     */
    public void deleteEntity(EntityController entity) {
        if (!(entity instanceof PlayerController)) {
            enemies.remove(entity);
            roomObjects.get(entity.getY()).set(entity.getX(), null);
            if (enemies.size() == 0) {
                Logger.getLogger(getClass().getName()).log(Level.INFO, "All enemies died opening doors...");
            }
        }
    }

    /**
     * Calls deleteEntity on all entities in the room. Used when the player dies to end all entity Threads.
     */
    public void killAllEnemies() {
        Logger.getLogger(getClass().getName()).log(Level.INFO, "Killing all enemies...");
        List<EntityController> copy = List.copyOf(enemies);     // You can't change the list you are going through
        for (EntityController entity : copy) {
            entity.changeStats(0, -entity.getHealth(), 0, 0D, 0);
        }
    }

    /**
     * Damages an GameObject on given coordinates if they are in bound and an EntityController.
     *
     * @param x      The x coordinate of the tile to be attacked.
     * @param y      The y coordinate of the tile to be attacked.
     * @param damage The amount of damage the tile is supposed to receive.
     *               (ex. If damage is 10 the entities health is lowered by 10)
     */
    public void attackGameObject(Integer x, Integer y, Integer damage) {
        if (isXYinBounds(x, y) && roomObjects.get(y).get(x) != null && roomObjects.get(y).get(x) instanceof EntityController) {
            ((EntityController) roomObjects.get(y).get(x)).changeStats(0, -damage, 0, 0D, 0);
        }
    }

    /**
     * Checks if the coordinates are in bound of the room and if the GameObject is Interactive. If it is it calls the
     * interact method of the Interactive GameObject.
     *
     * @param player The PlayerController that wants to interact with a tile.
     * @param x      The x coordinate of the tile to be interacted with.
     * @param y      The y coordinate of the tile to be interacted with.
     */
    public void interactWithGameObject(PlayerController player, Integer x, Integer y) {
        if (isXYinBounds(x, y)) {
            GameObject gameObject = roomObjects.get(y).get(x);
            if (gameObject instanceof Interactive) {
                ((Interactive) gameObject).interact(this, player);
            }
        }
    }

    /**
     * Places a GameObject in the room if the given index is in bound.
     *
     * @param object The GameObject which is to be placed.
     * @param x      The x coordinate where the object is to be placed.
     * @param y      The y coordinate where the object is to be placed.
     */
    public void placeGameObject(GameObject object, Integer x, Integer y) {
        if (isXYinBounds(x, y)) {
            roomObjects.get(y).set(x, object);
            if (object != null) {
                object.setX(x);
                object.setY(y);
            }
        }
    }

    /**
     * Looks above, below, to the right and to the left of this room and sets the exits of current room accordingly.
     *
     * @param roomsAlignment The alignment of rooms in a level.
     * @param roomX          The x coordinate of this room in the room alignment.
     * @param roomY          The y coordinate of this room in the room alignment.
     */
    public void setExits(List<List<Integer>> roomsAlignment, Integer roomX, Integer roomY) {
        if (roomY + 1 < roomsAlignment.size() && roomsAlignment.get(roomY + 1).get(roomX) != 0) {
            canExitDown = true;
        }
        if (roomY - 1 >= 0 && roomsAlignment.get(roomY - 1).get(roomX) != 0) {
            canExitUp = true;
        }
        if (roomX + 1 < roomsAlignment.get(roomY).size() && roomsAlignment.get(roomY).get(roomX + 1) != 0) {
            canExitRight = true;
        }
        if (roomX - 1 >= 0 && roomsAlignment.get(roomY).get(roomX - 1) != 0) {
            canExitLeft = true;
        }
        Logger.getLogger(getClass().getName()).log(Level.INFO, "Exits of room " + roomID + " set to [" +
                "up: " + canExitUp + ", down: " + canExitDown + ", right: " + canExitRight + ", left: " + canExitLeft + "]");
    }

    /**
     * @param x The x coordinate.
     * @param y The x coordinate.
     * @return if given coordinates are within the room bounds.
     */
    private Boolean isXYinBounds(Integer x, Integer y) {
        return y >= 0 && y < roomObjects.size() && x >= 0 && x < roomObjects.get(y).size();
    }

    public Integer getWidth() {
        return roomObjects.get(0).size();
    }

    public Integer getHeight() {
        return roomObjects.size();
    }

    public List<List<GameObject>> getRoomObjects() {
        return roomObjects;
    }

    public Integer getEnemiesSize() {
        return enemies.size();
    }

    public Boolean getCanExitLeft() {
        return canExitLeft;
    }

    public Boolean getCanExitRight() {
        return canExitRight;
    }

    public Boolean getCanExitUp() {
        return canExitUp;
    }

    public Boolean getCanExitDown() {
        return canExitDown;
    }

    public Integer getRoomID() {
        return roomID;
    }
}