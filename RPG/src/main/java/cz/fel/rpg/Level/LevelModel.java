package cz.fel.rpg.Level;

import cz.fel.rpg.GameObjects.Item;

import java.util.Collections;
import java.util.List;

/**
 * A model for the level. Holds Initialized rooms, the Alignment of rooms, the items that can appear in a level and the
 * coordinates of the starting room.
 */
public class LevelModel {
    private final List<List<Integer>> roomsAlignment;
    private final LevelController lvlController;
    private final List<Item> items;
    private final Integer levelID;
    private List<List<Room>> initializedRooms;
    private Integer startX;
    private Integer startY;

    /**
     * @param roomsAlignment  Alignments of rooms in the level.
     * @param items           All of the items that can be present in the level.
     * @param levelController The controller of the level.
     * @param levelID         ID of a given level.
     */
    public LevelModel(List<List<Integer>> roomsAlignment, List<Item> items, LevelController levelController, Integer levelID) {
        this.roomsAlignment = roomsAlignment;
        this.items = items;
        this.lvlController = levelController;
        this.levelID = levelID;
    }

    public List<List<Integer>> getRoomsAlignment() {
        return Collections.unmodifiableList(roomsAlignment);
    }

    public Integer getStartX() {
        return startX;
    }

    public Integer getStartY() {
        return startY;
    }

    public List<List<Room>> getInitializedRooms() {
        return initializedRooms;
    }

    public List<Item> getItems() {
        return items;
    }

    public Integer getLevelID() {
        return levelID;
    }

    public void setStartX(Integer startX) {
        this.startX = startX;
    }

    public void setStartY(Integer startY) {
        this.startY = startY;
    }

    public void setInitializedRooms(List<List<Room>> initializedRooms) {
        this.initializedRooms = initializedRooms;
    }

    @Override
    public String toString() {
        return "com.fel.rpg.Level{" +
                "roomsAlignment=" + roomsAlignment +
                ", startX=" + startX +
                ", startY=" + startY +
                '}';
    }
}
